Ansible Role to manage Let's Encrypt certificates
-------------------------------------------------

This simple role allow you to install certbot on a specific machine and generate
the certificates for the given domains.

## Example

```ansible-playbook
- hosts: all
  tasks:
    - name: Generate let's encrypt certificate for "domain.ltd"
      include_role:
        name: certbot
      vars:
        domain: 'domain.ltd'
        email: 'email@owner.ltd'
        renew:
          pre_hook: service nginx stop
          post_hook: service nginx start
```
